#!/bin/bash
# Created by the treker
# Date: 19.7.22
# Objective: check all Python files Syntax.



echo "Checking Python Files...."
sleep 1
python3 -m compileall web-app/app.py 2>&1
if [[ $? -eq 0 ]]
then
    echo "app.py has no errors"
else
    echo "app.py has errors" && exit 1
fi
sleep 1
python3 -m compileall web-app/app/__init__.py 2>&1
if [[ $? -eq 0 ]]
then
    echo "__init__.py has no errors"
else
    echo "__init__.py has errors" && exit 1 
fi
sleep 1
python3 -m compileall web-app/app/views.py 2>&1
if [[ $? -eq 0 ]]
then
    echo "views.py has no errors"
else
    echo "views.py has errors" && exit 1
fi



