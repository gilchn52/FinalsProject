from urllib import response
from app import app
import os
import sys
import logging
from dotenv import load_dotenv
load_dotenv()

logger = logging.getLogger(__name__)
logging.basicConfig(filename='app-active.log',level=logging.DEBUG,format='%(asctime)s %(levelname)-8s %(message)s',datefmt='%d-%m-%Y %H:%M:%S')

# the sys.path allows us to import file from the parent directory .

sys.path.insert(0, '..\..')

from flask import Flask, render_template, request, url_for, redirect
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

client = MongoClient(
    host=os.environ["DB_HOST"],
    username=os.environ["DB_USERNAME"],
    password=os.environ["DB_PASSWORD"],
    port=27017,
    authSource=os.environ["DB_AUTHSOURCE"],
)
try:
    client.admin.command('ismaster')
    logger.debug("connection to database succesfull")
except ConnectionFailure:
    logger.debug("Server not avaliable")
    
db = client["mydata"]
todos = db.todos

@app.route('/', methods=('GET', 'POST'))
def index():
    if request.method=='POST':
        Name = request.form['Name']
        Gender = request.form['Gender']
        Current_Weight = request.form['Current Weight']
        Desired_Weight = request.form['Desired Weight']
        Height = request.form['Height']
        Age = request.form['Age']
        Email = request.form['Email']
        todos.insert_one({'Name': Name, 'Gender': Gender, 'Current Weight': Current_Weight, 'Desired Weight': Desired_Weight, 'Height': Height, 'Age': Age, 'Email': Email})
        app.logger.info('Redirecting to myresult.html at /templates/myresults.html status:')
        return redirect(url_for('result'))  
    all_todos = todos.find()
    return render_template('index.html', todos=all_todos)

@app.route('/results')
def result():
    global thename,thegender,thecweight,thedweight,theheight,theemail,realage
    stud = []
    realgender = []
    realweight = []
    dweights = []
    theheight = []
    realage = []
    theemail = []
    mycol = db["todos"]
    
    for names in mycol.find():
        realname = "%s"%names['Name']
        stud.append(realname)
        
    for genders in mycol.find():
        realgender = "%s"%genders['Gender']
        stud.append(realgender)
        
    for cweights in mycol.find():
        realweight = "%s"%cweights['Current Weight']
        stud.append(realweight)
        
    for dweight in mycol.find():
        dweights = "%s"%dweight['Desired Weight']
        stud.append(dweights)
        
    for height in mycol.find():
        realheight = "%s"%height['Height']
        stud.append(realheight)
        
    for ages in mycol.find():
        realage = "%s"%ages['Age']
        stud.append(realage)
        
    for emails in mycol.find():
        realemail = "%s"%height['Email']
        stud.append(realemail)
    
    thename = realname
    thegender = realgender
    thecweight = realweight
    thedweight = dweights
    theheight = realheight
    theage = realage
    theemail = realemail
    recomend = ""
    weightint = int(thedweight)
    dweightint = int(thedweight)
    heightint = int(theheight)
    ageint = int(theage)
    bmiformula = (weightint / heightint / heightint)*10000
    bmicalculate = round(bmiformula,2)
    
    kgtopounds = weightint * 2.2046
    calories = (15 * kgtopounds)
    if bmicalculate < 18.5:
        message = "you are underweight !!!! do a fat up diet"
        recomend = "you need to eat more and consider going to a professional consulting"
    elif bmicalculate > 18 or bmicalculate < 24.9:
        message = " you are healthy ! , no need to do any kind of diet"
        recomend = "keep doing what you're doing :)"
    elif bmicalculate > 25 or bmicalculate < 29.9:
        message = "you are overweight consider doing diet asap"
        recomend = "walk at least 10000 steps a day"
    elif bmicalculate > 30:
        message = "you are not healthy, we highly recommends going to a professional consulting"
     
    return render_template('myresults.html', thename=thename, thegender=thegender, thecweight=thecweight, thedweight=thedweight, theheight=theheight, theage=theage, theemail=theemail, recomend=recomend, bmicalculate=bmicalculate, message=message, calories=calories )







