from app import app
import logging
logging.basicConfig(filename='app-init.log',level=logging.DEBUG)

port=5000
debug=True
if __name__ == "__main__":
    app.run(host='0.0.0.0',port=port,debug=debug)


