<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/gilchn52/FinalsProject.git">
    <img src="images/logo.png" alt="Logo" width="160" height="160">
  </a>

  <h3 align="center"> DevSecOps Finals Project </h3>

  <p align="center">
    the below program is based on Python Backend , And have a Custom templates i've Build as Frontend.
    Platform is Linux Based, NoSql Database based on MongoDB . 
    there are 3 stages to this program : Run Tests , Build & Push , and Finally Deploy the Application to K8S On Premsis Cluster. 
    The Pipeline is based on GitLab CI/CD. 
    
<br />

  </p>
</p>

<p align="left"> What The Program does: 
well in a nutshell , it asks for you physical attributes like age,weight,height etc.. and calculate if you need a diet. </p>





<!-- TABLE OF CONTENTS -->
## Table of Contents

* [Getting Started](#getting-started)
* [Notes](#Notes)




<!-- GETTING STARTED -->
## Getting Started

Running Tests: 
=============================

The first stage of the Pipeline is running the tests
which includes spellcheck for bash files,
and synthax check for python files.


Create Dind
=================
The second stage of the pipeline will create docker in docker image.
which enables us to deploy pods/deployments on our k8s cluster



Build The image and Push it to Docker Repo
===========================================


* assuming the test stage was a successfull.

* the pipeline will use the variables of the project to login onto docker hub . the creds are masked of course.
* the pipeline build stage does the below :
  1. build the image with current Dockerfile in the gitlab repo
  2. upload to the docker hub, with latest as the final version for the image. 



Deploy the image:
=================

The pipeline will connect to our K8S-Cluster and creates a virtual environment so we can deploy directly to k8s cluster
there you can add your manifest.yaml files .
seat and enjoy the sucessfull process.

to see if everything is o.k...:
===================================
just open a browser and type your http://{{ your cluster-ip}}:30006 and you're done
follow the submit form and see the results on the next page.

<!-- Notes -->
## Notes


* first you must create a public repository 
  you must first have a docker hub account on https://hub.docker.com/ and local repo that is public and not private.
  i've named my repository by name of: **igalcohen/devops**
  and myusername is **igalcohen** . so my repository name is : 

  **igalcohen/devops**

* you must have gitlab installed on your K8S Cluster

  afull instructions are avaliable on the gitlab website:
  https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html
  
  
  once we have an agent and our GitLab CI/CD is able to connect and deploy to our K8S Cluster.
  

  Best Regards
  "The Treker"